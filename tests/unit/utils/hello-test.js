import Hello from 'dummy/utils/hello';
import { module, test } from 'qunit';

module('Unit | Utility | hello');

test('hello typescript', function(assert) {
  let hello = new Hello('typescript');
  assert.equal(hello.greeting(), 'hello typescript');
});

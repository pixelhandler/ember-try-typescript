/* eslint-env node */
'use strict';
// const fs = require('fs');
// const path = require('path');
// const typescript = require('broccoli-typescript-compiler');
// const { find } = require('broccoli-stew');
// const merge = require('broccoli-merge-trees');

module.exports = {
  name: 'ember-try-typescript',

  skip_treeForAddon(tree) {
    let tsconfig = JSON.parse(fs.readFileSync(path.join(__dirname, 'tsconfig.json')));
    let compiledTypeScript = typescript(tree, { tsconfig: tsconfig });
    let modules = find(compiledTypeScript, {
      srcDir: 'addon',
      exclude: ['**/*.d.ts'],
      destDir: 'addon'
    });
    return this._super.treeForAddon.apply(this, merge([tree, modules]));
  }

};

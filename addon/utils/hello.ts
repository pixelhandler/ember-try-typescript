export default class Hello {
  [key: string]: any;

  private _name: string | undefined;

  constructor(name?: string) {
    this._name = name;
  }

  public greeting(): string {
    return `hello ${this._name}`;
  }
}